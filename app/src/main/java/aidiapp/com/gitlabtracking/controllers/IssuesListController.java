package aidiapp.com.gitlabtracking.controllers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eciaidiapp on 21/3/17.
 */

public class IssuesListController extends GenericRequest implements GenericRequest.Listener {
    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onSuccess(String result) {
        try {
            this.listener.onSuccess(new JSONArray(result));
        } catch (JSONException e) {
            e.printStackTrace();
            this.listener.onFailed();
        }
    }

    @Override
    public void onFailed() {
        this.listener.onFailed();
    }

    public interface Listener{
        void onSuccess(JSONArray list);
        void onFailed();

    }
    private Listener listener;

    public IssuesListController(Listener listener) {
        this.listener = listener;
        super.setListener(this);
    }

    @Override
    protected String doInBackground(String... params) {
        JSONObject account=StoredAccount.retrieveAccount();
        try {
            String host=account.getString("host");
            String privatetoken=account.getString("private_token");

            return super.doInBackground("GET",host+"/api/v3/todos?private_token="+privatetoken+"&action=assigned&type=Issue","");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}
