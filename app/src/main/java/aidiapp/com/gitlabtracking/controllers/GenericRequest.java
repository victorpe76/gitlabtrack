package aidiapp.com.gitlabtracking.controllers;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by eciaidiapp on 19/3/17.
 */

public class GenericRequest extends AsyncTask<String,Integer,String> {
    public Listener getBaseListener() {
        return baseListener;
    }

    public void setListener(Listener listener) {
        this.baseListener = listener;
    }

    public interface Listener{
        void onSuccess(String result);
        void onFailed();
    }
    private Listener baseListener;

    public GenericRequest() {
    }

    @Override
    protected String doInBackground(String... params) {
        String verb=params[0];
        String resource=params[1];
        String payload=params[2];

        URL url = null;
        try {
            url = new URL(resource);

            HttpURLConnection conn= (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(verb);
            if(verb.equals("POST")){
                conn.setDoOutput(true);
                //TODO: Añadir el cuerpo de la petición
            }
            conn.setDoInput(true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s.isEmpty()){
            this.baseListener.onFailed();
        }else{
            this.baseListener.onSuccess(s);
        }
    }
}
