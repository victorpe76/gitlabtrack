package aidiapp.com.gitlabtracking.controllers;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eciaidiapp on 20/3/17.
 */

public class StoredAccount {

    public static SharedPreferences prefs;

    public static void init(Context ctx){
        prefs=ctx.getApplicationContext().getSharedPreferences("ACCOUNTPREFS", Context.MODE_PRIVATE);
    }

    public static void setPrefs(SharedPreferences prefs) {
        StoredAccount.prefs = prefs;
    }
    public static void storeAccount(JSONObject account){
        SharedPreferences.Editor ed=prefs.edit();
        ed.putString("STOREDACCOUNT",account.toString());
        ed.commit();
    }
    public static JSONObject retrieveAccount(){
        String cad=prefs.getString("STOREDACCOUNT","EMPTY");
        if(cad.equals("EMPTY")){
            return null;
        }else{
            try {
                JSONObject r=new JSONObject(cad);
                if(!r.has("host")){
                    r.put("host","https://gitlab.com");
                }
                return r;
            } catch (JSONException e) {
                return null;
            }
        }
    }


}
