package aidiapp.com.gitlabtracking.controllers;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by eciaidiapp on 24/3/17.
 */

public class StatsCalcController extends AsyncTask<JSONArray,Integer,JSONObject> {

    public StatsCalcControllerListener getListener() {
        return listener;
    }

    public void setListener(StatsCalcControllerListener listener) {
        this.listener = listener;
    }

    public interface StatsCalcControllerListener{
        void onResult(JSONObject result);
        void onFailed();
    }

    private StatsCalcControllerListener listener;

    public StatsCalcController(StatsCalcControllerListener listener) {
        this.listener = listener;
    }

    @Override
    protected JSONObject doInBackground(JSONArray... params) {
        JSONArray milestones=params[0];
        JSONObject account=StoredAccount.retrieveAccount();
        long total=0;
        JSONArray stats_per_milestone=new JSONArray();
        for(int n=0;n<milestones.length();n++){
            try {
                JSONObject milestonestats = getTimeStatsPerMilestone(milestones.getJSONObject(n), account.getString("host"), account.getString("private_token"));
           stats_per_milestone.put(milestonestats);
                total+=milestonestats.getLong(milestones.getJSONObject(n).getString("title"));
            }catch(Exception ex){

            }

        }
        JSONObject result=new JSONObject();
        try {
            result.put("total_time_spent",total);
            result.put("total_per_milestone",stats_per_milestone);
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private JSONObject getTimeStatsPerMilestone(JSONObject milestone, String host, String private_token) throws IOException, JSONException {
        JSONObject statpermilestone=new JSONObject();

        long total_time_spent=0;
        JSONArray issues=this.getIssueList(milestone,host,private_token);
        for(int n=0;n<issues.length();n++){
            JSONObject stats=this.getTimeStats(issues.getJSONObject(n),host,private_token);
            total_time_spent+=stats.getLong("total_time_spent");
        }

        statpermilestone.put(milestone.getString("title"),total_time_spent);
        return statpermilestone;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if(jsonObject!=null){
            this.listener.onResult(jsonObject);
        }else{
            this.listener.onFailed();
        }
    }

    private JSONArray getIssueList(JSONObject milestone,String host,String privatetoken) throws IOException, JSONException {

        URL url = new URL(host+"/api/v3/projects/"+milestone.getString("project_id")
                +"/issues?milestone="+ URLEncoder.encode(milestone.getString("title"),"UTF-8")+
                "&private_token="+privatetoken);
        HttpURLConnection conn= (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);

        }
        in.close();
        return new JSONArray(response.toString());
    }
    private JSONObject getTimeStats(JSONObject issue, String host, String privatetoken) throws JSONException, IOException {


        URL url = new URL(host+"/api/v3/projects/"+issue.getString("project_id")+"/issues/"+issue.getString("id")+
                "/time_stats?private_token="+privatetoken);
        HttpURLConnection conn= (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return new JSONObject(response.toString());
    }
}
