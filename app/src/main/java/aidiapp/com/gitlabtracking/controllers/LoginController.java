package aidiapp.com.gitlabtracking.controllers;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eciaidiapp on 18/3/17.
 */

public class LoginController extends GenericRequest implements GenericRequest.Listener {


    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onSuccess(String result) {
        try {
            this.listener.onSigned(new JSONObject(result));
        } catch (JSONException e) {
            e.printStackTrace();
            this.listener.onFailed();
        }
    }

    @Override
    public void onFailed() {

    }

    public interface Listener{
        void onSigned(JSONObject user);
        void onFailed();

    }

    private Listener listener;

    public LoginController(Listener listener) {
        this.listener = listener;
        super.setListener(this);


    }



}
