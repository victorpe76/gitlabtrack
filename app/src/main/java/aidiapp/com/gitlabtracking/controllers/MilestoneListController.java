package aidiapp.com.gitlabtracking.controllers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eciaidiapp on 21/3/17.
 */

public class MilestoneListController extends GenericRequest implements GenericRequest.Listener {
    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onSuccess(String result) {
        try {
            this.listener.onMilestoneListSuccess(new JSONArray(result));
        } catch (JSONException e) {
            e.printStackTrace();
            this.listener.onMilestoneListFailed();
        }
    }

    @Override
    public void onFailed() {
        this.listener.onMilestoneListFailed();
    }

    public interface Listener{
        void onMilestoneListSuccess(JSONArray list);
        void onMilestoneListFailed();

    }
    private Listener listener;

    public MilestoneListController(Listener listener) {
        this.listener = listener;
        super.setListener(this);
    }

    @Override
    protected String doInBackground(String... params) {
        String projectid=params[0];
        JSONObject account=StoredAccount.retrieveAccount();
        try {
            String host=account.getString("host");
            String privatetoken=account.getString("private_token");

            return super.doInBackground("GET",host+"/api/v3/projects/"+projectid+"/milestones?private_token="
                    +privatetoken,"");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}
