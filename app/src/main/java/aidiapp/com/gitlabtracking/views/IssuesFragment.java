package aidiapp.com.gitlabtracking.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import aidiapp.com.gitlabtracking.R;
import aidiapp.com.gitlabtracking.controllers.IssuesListController;
import aidiapp.com.gitlabtracking.controllers.LoginController;
import aidiapp.com.gitlabtracking.controllers.StoredAccount;
import aidiapp.com.gitlabtracking.views.adapters.IssueAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class IssuesFragment extends Fragment implements IssuesListController.Listener {


    private RecyclerView rvIssues;
    private LinearLayoutManager mLayoutManager;

    public IssuesFragment() {
        // Required empty p
        // ublic constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        IssuesListController lc=new IssuesListController(this);


            lc.execute();

//
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_issueslist, container, false);
        this.refChildren(v);
        return v;
    }

    private void refChildren(View v) {
        this.rvIssues= (RecyclerView) v.findViewById(R.id.rvIssues);
        mLayoutManager = new LinearLayoutManager(this.getContext());
        rvIssues.setLayoutManager(mLayoutManager);

    }


    @Override
    public void onSuccess(JSONArray list) {
        IssueAdapter iadapter=new IssueAdapter(list);
        this.rvIssues.setAdapter(iadapter);
    }
    @Override
    public void onFailed() {

    }
}
