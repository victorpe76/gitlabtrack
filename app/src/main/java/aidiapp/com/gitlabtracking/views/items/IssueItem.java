package aidiapp.com.gitlabtracking.views.items;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import aidiapp.com.gitlabtracking.R;

/**
 * Created by eciaidiapp on 21/3/17.
 */

public class IssueItem extends CardView implements View.OnClickListener {
    private TextView tvproject;
    private TextView tvissue;
    private TextView tvDescription;
    private ImageButton ibShowMore;
    private JSONObject todoissue;

    public IssueItem(Context context) {
        super(context);
        init();
    }

    private void init() {
        inflate(this.getContext(), R.layout.issueitem,this);

        this.refChildren();

    }

    private void refChildren() {
        this.tvproject=(TextView)this.findViewById(R.id.tvProject);
        this.tvissue=(TextView)this.findViewById(R.id.tvIssueName);
        this.tvDescription=(TextView)this.findViewById(R.id.tvDescription);
        this.ibShowMore=(ImageButton)this.findViewById(R.id.ibShowMore);
        this.ibShowMore.setOnClickListener(this);
    }

    public IssueItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IssueItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setTODOIssue(JSONObject j){
        this.todoissue=j;
        this.refreshContent();
    }

    private void refreshContent() {
        try {
            this.tvproject.setText(this.todoissue.getJSONObject("project").getString("name_with_namespace"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            this.tvissue.setText(this.todoissue.getJSONObject("target").getString("title"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.tvDescription.setText(this.todoissue.getJSONObject("target").getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.ibShowMore){
            this.tvDescription.setVisibility(View.VISIBLE);
        }
    }
}
