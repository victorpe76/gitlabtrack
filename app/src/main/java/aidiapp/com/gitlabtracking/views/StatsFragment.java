package aidiapp.com.gitlabtracking.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import aidiapp.com.gitlabtracking.R;
import aidiapp.com.gitlabtracking.controllers.IssuesListController;
import aidiapp.com.gitlabtracking.controllers.MilestoneListController;
import aidiapp.com.gitlabtracking.controllers.ProjectListController;
import aidiapp.com.gitlabtracking.controllers.StatsCalcController;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatsFragment extends Fragment implements ProjectListController.Listener, AdapterView.OnItemSelectedListener, MilestoneListController.Listener, StatsCalcController.StatsCalcControllerListener {


    private static final String TAG = "STATSFRAGMENT";
    private JSONArray projects;
    private Spinner projectspinner;
    private JSONArray milestonelist;

    public StatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_stats, container, false);

        this.refChildren(v);
        return v;
    }

    private void refChildren(View v) {
        this.projectspinner=(Spinner)v.findViewById(R.id.projectSpinner);
        this.projectspinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        ProjectListController ilc=new ProjectListController(this);
        ilc.execute();
    }

    @Override
    public void onSuccess(JSONArray list) {
        this.projects=list;
        ArrayList<String> projectNames=new ArrayList();
        for(int n=0;n<list.length();n++){
            try {
                JSONObject proj=list.getJSONObject(n);
                projectNames.add(proj.getString("name_with_namespace"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(),
                android.R.layout.simple_spinner_item, projectNames);
        this.projectspinner.setAdapter(adapter);

    }

    @Override
    public void onResult(JSONObject result) {
        Log.d(TAG,"El resultado es "+result.toString());
    }

    @Override
    public void onFailed() {
        Log.d(TAG,"Hemos fallado");
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

        try {
            String projid=this.projects.getJSONObject(position).getString("id");
            MilestoneListController mlc=new MilestoneListController(this);
            mlc.execute(projid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onMilestoneListSuccess(JSONArray list) {
        this.milestonelist=list;
        StatsCalcController scc=new StatsCalcController(this);
        scc.execute(list);
    }

    @Override
    public void onMilestoneListFailed() {

    }
}
