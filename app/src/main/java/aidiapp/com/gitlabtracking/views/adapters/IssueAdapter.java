package aidiapp.com.gitlabtracking.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import aidiapp.com.gitlabtracking.views.items.IssueItem;

/**
 * Created by eciaidiapp on 21/3/17.
 */

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.ViewHolder> {
    private JSONArray values;

    public IssueAdapter(JSONArray values) {
        this.values = values;
    }

    @Override
    public IssueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        IssueItem v=new IssueItem(parent.getContext());
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,10,10,10);
        v.setLayoutParams(lp);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(IssueAdapter.ViewHolder holder, int position) {
        try {
            holder.item.setTODOIssue(this.values.getJSONObject(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.values.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public IssueItem item;
        public ViewHolder(IssueItem itemView) {

            super(itemView);
            item=itemView;
        }
    }
}
